from math import exp
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve


########################
########################
#      USER INPUT      #

#### TIME RANGE ######
t0 = 0
tend = 50
dt = 1e-2
trange = np.linspace(t0, tend, int(((tend-t0)/dt)+1))
##### Parameters ####
VarParamStart = 2
VarParamEnd = 0
Iterations = 21
VarParamRange = np.linspace(VarParamStart,VarParamEnd,Iterations)

P = [-1]         ### This is Parameters values without Variable Parameter
#print(type(P))
####VARIABLES#######
Order = 2   #Order of ODE
U0 = [1,1]  #length matches Order
PhCVar = 1   ## For when duXdt = 0 is phase condition, PhCVar = X here.



#########################
#########################
#         ODEs          #

##### PREDPREY FUNC #########
def PredPrey(VarVals,t,Params):
	x, y = VarVals

	a,d = Params[0:-1] ## NOT Variable Params      #### This is janky
	b = Params[-1] ## Variable Param

	dxdt = x*(1-x)-(a*x*y)/(d+x)
	dydt = b*y*(1-y/x)
	ddt = np.array([dxdt,dydt])
	return ddt

##### HOPFBI FUNC ######
def HopfBi(VarVals,t,Params):
	u1,u2 = VarVals
	sigma = Params[0]
	beta = Params[-1]
	du1dt = beta*u1 - u2 + sigma*u1*(u1**2 + u2**2)
	du2dt = u1 + beta*u2 + sigma*u2*(u1**2 + u2**2)
	ddt = np.array([du1dt,du2dt])
	return ddt

##### PHASE COND GUESSING ########
def shoot(U0Tguess,*args):
	ODE,P,PhaseCond,a,PhaseCVar = args
	UShootVals = odeint(ODE, U0Tguess[0:-1], np.linspace(t0,U0Tguess[-1],2),args=(P,))
	
	if PhaseCond == True:
		phaseC = [ODE(U0Tguess[0:-1],0,P)[PhaseCVar-1]]
	else:
		phaseC = U0Tguess[0] - a
	
	### ERRORS ###
	Errors = np.zeros(Order)
	for i in range(Order):
		Errors[i] = UShootVals[1,i] -UShootVals[0,i]
	shootreturn = list(Errors) + phaseC
	return shootreturn


################### SETTINGS ###################
ODE = HopfBi
PhaseCondition = True ##True if PhC is duXdt=0, false if Constant PhCguess...
PhCguess = 0.4
Tguess = 5
PlotODESimulated = True
NatParamCont = True ##True if NatParamCont should be executed for VarParam stated in user input
PlotNatPCont = True


#####################
#####################
#### MAIN FUNC ######
def main():

	global PlotODESimulated
	global P
	global VarParamRange
	global ODE
	global U0
	global trange
	global PhaseCondition
	global PhCguess
	global PhCVar
	global NatParamCont
	global Order
	
	#Initial Solve and plot#
	if PlotODESimulated == True:
		Ps = P+[VarParamRange[20]] #This is Parameter list with variable parameter added with 50th value
		UVals = odeint(ODE, U0, trange, args=(Ps,))
		
		plt.plot(trange,UVals)
		plt.show()

	#Natural Parameter Continuation for loop over VarParamRange
	if NatParamCont == True:
		NaturalParamContSols = np.zeros((len(VarParamRange),Order+1))
		for i in range(len(VarParamRange)):
			
			if i == 0:
				P = P+[VarParamRange[i]]
				U0T = U0+[Tguess]
			else:
				P[-1] = VarParamRange[i]
				U0T = NaturalParamContSols[i-1]
				
			THISSTUFF = (ODE,P,PhaseCondition,PhCguess,PhCVar)	
			UTSols = fsolve(shoot,U0T,args=(THISSTUFF))
			NaturalParamContSols[i] = UTSols
			
		if PlotNatPCont == True:
			maxvals = np.zeros(len(NaturalParamContSols))
			for i in range(len(NaturalParamContSols)):
				
				P[-1] = VarParamRange[i]
				PeriodTRange = np.linspace(t0, NaturalParamContSols[i,-1], 101)
				UValues = odeint(ODE, NaturalParamContSols[i,0:-1],PeriodTRange , args=(P,))

				SumSQ = np.zeros(len(UValues))
				for j in range(len(UValues)):

					SumSQ[j] = np.linalg.norm(UValues[j])
				maxvals[i] = np.max(SumSQ)
		
		
		
			plt.plot(VarParamRange,maxvals)
			plt.show()
	
	print(NaturalParamContSols)
	
	
			
	
	return NaturalParamContSols

main()

