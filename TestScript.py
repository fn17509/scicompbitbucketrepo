from math import exp
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
from math import sin
from math import cos

from NumShootFuncs import simulate
from NumShootFuncs import shoot
from NumShootFuncs import shootsolve
from NumShootFuncs import naturalparamcont



#####################################
#####################################
#			Test script				#
#	
#	Running this .py file with no cmd line arguments will execute:
#	-A test on the shoot() funtion in NumShootFuncs.py, comparing the 
#	 output of the Hopf Bifurcation normal form, HopfBi(), with its known
#	 explicit solution values.   
#	-A similar test on shoot(), but 3 dimensional ############NOT DONE YET###################### 

def HopfBiExplicitU1(t,beta,theta):
	u1 = np.sqrt(beta)*cos(t+theta)
	return u1 

def HopfBiExplicitU2(t,beta,theta):
	u2 = np.sqrt(beta)*sin(t+theta)
	return u2
	
def HopfBiExplicitU3(t,beta,theta):
	u3 = 0
	return u3

def HopfBiTest(ODE,ODESolver,Order,U0,Tguess,P,VarParamRange,VarParamTestvalue,PhCondition,PhCguess,PhCVar):
	
	theta = 0
	beta = VarParamTestvalue
	shootfunc = shoot
	U0T = U0+[Tguess]
	P = P+[VarParamTestvalue]
	LimitCycleSol = shootsolve(shootfunc,U0T,ODE,ODESolver,Order,P,PhCondition,PhCguess,PhCVar)
	Trange = np.linspace(0,LimitCycleSol[-1],101)	
	
	if ODE == HopfBi:
		ExplicitSols = [HopfBiExplicitU1(LimitCycleSol[-1],beta,theta),HopfBiExplicitU2(LimitCycleSol[-1],beta,theta)]
	if ODE == HopfBi3D:
		ExplicitSols = [HopfBiExplicitU1(LimitCycleSol[-1],beta,theta),HopfBiExplicitU2(LimitCycleSol[-1],beta,theta),HopfBiExplicitU3(LimitCycleSol[-1],beta,theta)]
	a = np.allclose(LimitCycleSol[0:-1],ExplicitSols,2,1e-8)
	if a == True:
		print("Test Passed! Limit cycle solutions are adequately accurate.")
	else:
		print("Test Failed! Limit cycle solutions are not adequately accurate.")
		
	return 0


		
def HopfBi(VarVals,t,Params):
	u1,u2 = VarVals
	
	sigma = Params[0]
	beta = Params[-1]
	
	du1dt = beta*u1 - u2 + sigma*u1*(u1**2 + u2**2)
	du2dt = u1 + beta*u2 + sigma*u2*(u1**2 + u2**2)
	ddt = np.array([du1dt,du2dt])
	return ddt
	
def HopfBi3D(VarVals,t,Params):
	u1,u2,u3 = VarVals
	
	sigma = Params[0]
	beta = Params[-1]
	
	du1dt = beta*u1 - u2 + sigma*u1*(u1**2 + u2**2)
	du2dt = u1 + beta*u2 + sigma*u2*(u1**2 + u2**2)
	du3dt = -u3
	ddt = np.array([du1dt,du2dt,du3dt])
	return ddt




################################################
################### SETTINGS ###################
ODE = HopfBi     #Name of ODE function to be used
ODESolver = odeint  #Name of ODE solving function to be used by shoot(), simulate(), naturalparamcont()

PhCondition = True       ##   True if PhC is duXdt=0,
PhCguess = 0.4              ##   False if Constant PhCguess...

Tguess = 4    ## Guess at time period of limit cycle

#PlotODESimulated = True ##	True if a plot of initial conditions is wanted

#NatParamCont = True ##	True if NatParamCont should be executed for VarParam stated in user input
#PlotNatPCont = True ##	True if plot of magnitude ||U|| against VarParam is wanted, to show limit cycle branch


#### TIME RANGE ######
# Defined for simulating ODE over trange, with U0 initial conditions

t0 = 0
tend = 15
dt = 1e-2
trange = np.linspace(t0, tend, int(((tend-t0)/dt)+1))


##### Parameters ####
VarParamStart = 2       # 
VarParamEnd = 0.1         # Define Variable Parameter Range here
Iterations = 101
VarParamRange = np.linspace(VarParamStart,VarParamEnd,Iterations)
VarParamTestvalue = VarParamRange[0]   #Choice of Variable Parameter value for test

P = [-1]         #Non Variable Parameter Values, in list. Unpacked in ODE definition


####VARIABLES#######
ODE = HopfBi
Order = 2   #Order of ODE
U0 = [1,0.1]  #ODE Variable initial conditions    (length matches Order)
PhCVar = 1   ## For when duXdt = 0 is phase condition, PhCVar = X here.




HopfBiTest(ODE,ODESolver,Order,U0,Tguess,P,VarParamRange,VarParamTestvalue,PhCondition,PhCguess,PhCVar)


##### 3D Test #####
Order = 3
ODE = HopfBi3D
U0 = [0.5,0.5,0.5]
HopfBiTest(ODE,ODESolver,Order,U0,Tguess,P,VarParamRange,VarParamTestvalue,PhCondition,PhCguess,PhCVar)
