from math import exp
import matplotlib.pyplot as plt
import numpy as np

#for i in range(tend/dt):
#	t = t0
#	XYvalues.append(stepfunc(x0,y0,t))
			

a = 1
d = 0.1
b = 0.1
#b = np.linspace(0.1,0.5,0.01)

x0 = 0.4
y0 = 0.4
t0 = 0
tend = 10
dt = 1e-3

def func(xy,t):
	x, y = xy
	dxdt = x*(1-x)-(a*x*y)/(d+x)
	dydt = b*y*(1-y/x)
	ddt = np.array([dxdt,dydt])
	return ddt


def eulerstep(x1, y1, t1, h):
	x2 = x1 + h*xfunc(x1,y1,t1)
	y2 = y1 + h*yfunc(x1,y1,t1)
	return x2,y2
	
def RK4step(f,X,t1,h):
	k1 = h*f(X,t1)
	k2 = h*f(X + k1/2, t1 + h/2)
	k3 = h*f(X + k2 / 2, t1 + h / 2)
	k4 = h*f(X + k3, t1 + h/2)

	X2 = X +(1/6)*(k1 + 2*k2 + 2*k3 + k4)
	return X2

def solutionboundary(func,x1,y1,t1,t2,hmax):
	t=t1
	xy = np.array([x1,y1])
	sols = []
	while t < t2:
		h = min(t2-t, hmax)
		xy = RK4step(func,xy,t,h)
		sols.append(xy)
		t+=h
	return sols


XYvalues = []
#stepsize = []
#errors = []

def main():
	XYvalues = (solutionboundary(func, x0, y0, t0, tend, dt))
		
	plt.plot(np.linspace(t0,tend, len(XYvalues)),XYvalues)
	plt.show()
	return 0

main()
	
#	stepsize.append(hmax)
#	errors.append(err)
#	print("With stepsize {0}, the error is {1}".format(hmax, err))

#print(stepsize)
#print(errors)

#x = np.linspace(min(stepsize), max(stepsize), len(stepsize))
#y = np.linspace(min(errors),max(errors), len(errors))
#fig = plt.figure()
#ax = fig.add_subplot(1,1,1)
#ax.scatter(x,y)
#plt.show()

#if __name__ == "__main__":
#	import sys
#	main(*sys.argv[1:])