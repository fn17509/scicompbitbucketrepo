from math import exp
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve
#############
#### TIME RANGE ######
t0 = 0
tend = 100
dt = 1e-3
trange = np.linspace(t0, tend, int(((tend-t0)/dt)+1))

##### PREDPREY VARIABLES ####
a = 1
d = 0.1
b = 0.2

x0 = 0.4
y0 = 0.4
initvalsPredPrey = [x0,y0]

##### PREDPREY FUNC #########
def PredPrey(xy,t):
	x, y = xy
	dxdt = x*(1-x)-(a*x*y)/(d+x)
	dydt = b*y*(1-y/x)
	ddt = np.array([dxdt,dydt])
	return ddt

##### HOPFBI VARIABLES ######
u1 = 1
u2 = 1
initvalsHopfBi = [u1,u2]

beta = 1
sigma = -1

##### HOPFBI FUNC ######
def HopfBi(u0,t):
	u1,u2 = u0
	du1dt = beta*u1 - u2 + sigma*u1*(u1**2 + u2**2)
	du2dt = u1 + beta*u2 + sigma*u2*(u1**2 + u2**2)
	ddt = np.array([du1dt,du2dt])
	return ddt

##### PHASE COND GUESSING ########
def Guess(XYTguess, a):
	XYs = odeint(ODE, XYTguess[0:2], np.linspace(t0,XYTguess[2],2))
	
	phaseC = ODE(XYTguess[0:2],0)[0]
	
	#phaseC = XYTguess[0] - a
	
	Xerror = XYs[1,0] -XYs[0,0]
	Yerror = XYs[1,1] -XYs[0,1]
	return [Xerror, Yerror, phaseC]

################### SETTINGS ###################
ODE = PredPrey
initvals = initvalsPredPrey
PhCguess = 0.4
Tguess = [20]

#### MAIN FUNC ######
def main():
	#Initial Solve and plot#
	XYVals = odeint(ODE, initvals, trange)
	plt.plot(trange,XYVals)
	plt.show()
	
	#Phase Condition and Period Finding   SHOOTING
	XYT = initvals+Tguess
	XYTSols = fsolve(Guess,XYT,args=(PhCguess))
	print(XYTSols)
	
	#Plotting new Phase Conditionned sol
	XYPhaseCycle = odeint(ODE, XYTSols[0:2], trange)
	plt.plot(trange,XYPhaseCycle)
	plt.show()

	return 0
	
main()







################# MISC #############################

#XYvalues = (solutionboundary(ODE, x0, y0, t0, tend, dt))
	#plt.plot(np.linspace(t0,tend, len(XYvalues)),XYvalues)

# def RK4step(f,X,t1,h):
# 	k1 = h*f(X,t1)
# 	k2 = h*f(X + k1/2, t1 + h/2)
# 	k3 = h*f(X + k2 / 2, t1 + h / 2)
# 	k4 = h*f(X + k3, t1 + h/2)

# 	X2 = X +(1/6)*(k1 + 2*k2 + 2*k3 + k4)
# 	return X2

# def solutionboundary(Sfunc,x1,y1,t1,t2,hmax):
# 	t=t1
# 	xy = np.array([x1,y1])
# 	sols = []
# 	while t < t2:
# 		h = min(t2-t, hmax)
# 		xy = RK4step(Sfunc,xy,t,h)
# 		sols.append(xy)
# 		t+=h
# 	return sols
	
# maxminX = []
# maxminY = []	
	
	##Find period T
	
	# for i in range(len(XYalt)):
		# if ODE(XYalt[i], trange[i])[0] > -0.00001 and ODE(XYalt[i], trange[i])[0] < 0.00001:
			# maxminX.append(trange[i])
		# if ODE(XYalt[i], trange[i])[1] > -0.00001 and ODE(XYalt[i], trange[i])[1] < 0.00001:
			# maxminY.append(trange[i])
	
	# UniqueXmaxmin = np.unique(np.round(maxminX,1))
	# UniqueYmaxmin = np.unique(np.round(maxminY,1))
	
	# Tlistx = []
	# Tlisty = []
	
	# for i in range(len(UniqueXmaxmin)-2):
		# T = UniqueXmaxmin[i+2] - UniqueXmaxmin[i]
		# Tlistx.append(T)
		
	# for i in range(len(UniqueYmaxmin)-2):
		# T = UniqueYmaxmin[i+2] - UniqueYmaxmin[i]
		# Tlisty.append(T)
	
	# TX = np.mean(Tlistx)
	# TY = np.mean(Tlisty)
	# TFinal = (TX+TY)/2
	# print(TFinal)
	




