from math import exp
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve



################################################################################
################################################################################

                    #      FUNCTIONS FILE      #


################################################################################
################################################################################

##### PHASE COND GUESSING ########
def simulate(ODE,ODESolver,U0,trange,Params):
	"""
	A function that simulates a set of 1st order ODEs over a specified time range for some initial 
	conditons.
	
	Parameters
	---------
	ODE : The ODE to simulate. Should be defined as follows:

		def ODEName(VarVals,t,Params):
			unpack variables = VarVals
			unpack nonvariable params = Params[0:-1]
			unpack variable param = Params[-1]

			duXdt = equations   (x Order of ODE)

			ddt = np.array([all duXdts])
			return ddt

	ODESolver: Name of ODE solving function to be used for simulation. E.g odeint

	U0: List of initial conditions for Variables of ODE U1,U2 ... etc
		(...UN for N dimensional ODE set)		e.g [0.1,1.0]
	
	trange: np.linspace() defining time range of simulation, start point, stop point, and steps

	Params: List of parameter values to be unpacked by the ODE func.
	
	
	Returns
	---------
	Returns a numpy.array containing the columns of values of the variables U1,U2 etc. where each row is a time step in trange. 
	"""

	UVals = ODESolver(ODE, U0, trange, args=(Params,))
	plt.plot(trange,UVals)
	plt.show()
	return UVals




def shoot(U0Tguess,*args):
	"""
	A function that uses numerical shooting to find limit cycles of a specified ODE.

	Parameters
	---------
	U0Tguess: List of initial conditions for Variables of ODE U1,U2 ... etc
 			  concatenated with a guess of Time period T.    e.g [0.1, 1.0, 5]
	
	*args: arguments to be unpacked for shooting function.
		ODE:			The ODE to simulate. Defined as above in simulate().
		ODESolver:		Name of ODE solving function to be used for simulation. E.g odeint   #NEEDS TO BE IMPORTED BY USER
		Order:			Integer value, the dimensions of the ODE set i.e 2 for two first order ODE equations.
		P:				List of parameters for ODE. Including variable parameter as last entry.
		PhaseCond:		Boolean value: True for setting phase condition as duXdt = 0 where X defines which variable U1,U2 etc is used.
									   False for setting phase condition as a constant, PhaseCguess
		PhaseCguess:	Float value, constant to be used as phase condition if chosen.
		PhaseCVar:		Integer value, X where duXdt = 0 for phase condition.  (Choses which variable to use for phase condition)
	
	
	Returns
	---------
	Returns a numpy.array containing the variable value errors (i.e UX(T) - UX(0) )
	and the Phasecondition solution: either value of duXdt at t=0 or value of ( UX(0) - constant ) for constant phase condition.
	e.g [Xerror, Yerror, PhaseConditionvalue]
	"""

	ODE,ODESolver,Order,P,PhaseCond,PhaseCguess,PhaseCVar = args
	UShootVals = ODESolver(ODE, U0Tguess[0:-1], np.linspace(0,U0Tguess[-1],2),args=(P,))
	
	if PhaseCond == True:
		phaseC = [ODE(U0Tguess[0:-1],0,P)[PhaseCVar-1]]
	else:
		phaseC = U0Tguess[PhaseCVar-1] - PhaseCguess
	
	### ERRORS ###
	Errors = np.zeros(Order)
	for i in range(Order):
		Errors[i] = UShootVals[1,i] - UShootVals[0,i]
	shootreturn = list(Errors) + phaseC
	
	return shootreturn
	
def shootsolve(shootfunc,U0Tguess,ODE,ODESolver,Order,P,PhCondition,PhCguess,PhCVar):
	"""
	A function that root finds the shoot function, to find the limit cycle corrected intiial conditions
	and time period T.
	
	Parameters
	---------
	shootfunc:		The descretisation function, that will be solved for roots.
	U0Tguess:		List of initial conditions for Variables of ODE U1,U2 ... etc concatenated with Tguess e.g [0.1, 0.1, 10]
	ODE:			The ODE to simulate. Defined as above in simulate().
	ODESolver:		Name of ODE solving function to be used for simulation. E.g odeint
	Order:			Integer value, the dimensions of the ODE set i.e 2 for two first order ODE equations.
	P:				List of parameters for ODE.
	PhCondition:		Boolean value: True for setting phase condition as duXdt = 0 where X defines which variable U1,U2 etc is used.
								   False for setting phase condition as a constant, PhaseCguess
	PhCguess:		Float value, constant to be used as phase condition if chosen.
	PhCVar:			Integer value, X where duXdt = 0 for phase condition.  (Choses which variable to use for phase condition)

	
	Returns
	---------
	LimitCycleSol:	np.array of limit cycle solution for the U0Tguess. e.g [U1value, U2value, TimePeriod]
	"""
	
	shootargs = (ODE,ODESolver,Order,P,PhCondition,PhCguess,PhCVar)
	LimitCycleSol = fsolve(shootfunc,U0Tguess,args=(shootargs))
	
	return LimitCycleSol


def naturalparamcont(ODE,ODESolver,Order,U0,Tguess,P,VarParamRange,PhCondition,PhCguess,PhCVar,Plot):
	"""
	A function that executes numerical parameter continuation on an ODE set, given a variable parameter range and initial conditions.
	
	Parameters
	---------
	ODE:			The ODE to simulate. Defined as above in simulate().
	ODESolver:		Name of ODE solving function to be used for simulation. E.g odeint
	Order:			Integer value, the dimensions of the ODE set i.e 2 for two first order ODE equations.
	U0:				List of initial conditions for Variables of ODE U1,U2 ... etc
	Tguess:			A guess at the time period T of the oscillatory ODEs limit cycle.
	P:				List of parameters for ODE.
	VarParamRange:	np.linspace() defining the range of the variable parameter, and steps within range.
	PhCondition:		Boolean value: True for setting phase condition as duXdt = 0 where X defines which variable U1,U2 etc is used.
								   False for setting phase condition as a constant, PhaseCguess
	PhCguess:		Float value, constant to be used as phase condition if chosen.
	PhCVar:			Integer value, X where duXdt = 0 for phase condition.  (Choses which variable to use for phase condition)
	Plot:			Boolean: True if user wants a magnitude vs variable parameter plot, to show limit cycle branch of the variable parameter.
	
	Returns
	---------
	Sols:			np.array of limit cycle solutions for the VarParamRange. e.g row of Sols: [U1value, U2value, TimePeriod] 
	"""
	Sols = np.zeros((len(VarParamRange),Order+1))

	for i in range(len(VarParamRange)):
		if i == 0:
			P = P+[VarParamRange[i]]
			U0T = U0+[Tguess]
		else:
			P[-1] = VarParamRange[i]
			U0T = Sols[i-1]
		
		UTSols = shootsolve(shoot,U0T,ODE,ODESolver,Order,P,PhCondition,PhCguess,PhCVar)
		Sols[i] = UTSols

	if Plot == True:

		maxvals = np.zeros(len(Sols))

		for i in range(len(Sols)):

			P[-1] = VarParamRange[i]
			PeriodTRange = np.linspace(0, Sols[i,-1], 101)
			UValues = ODESolver(ODE, Sols[i,0:-1],PeriodTRange , args=(P,))

			SumSQ = np.zeros(len(UValues))

			for j in range(len(UValues)):
 				SumSQ[j] = np.linalg.norm(UValues[j])

			maxvals[i] = np.max(SumSQ)

		plt.plot(VarParamRange,maxvals)
		plt.show()
	return Sols

