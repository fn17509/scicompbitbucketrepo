from math import exp
import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import fsolve


from NumShootFuncs import simulate
from NumShootFuncs import shoot
from NumShootFuncs import naturalparamcont


################################################################################
################################################################################

                    #      USER INPUT FILE      #


################################################################################
################################################################################

#########################
#########################
#         ODEs          #

#   This section defines ODEs in first order form.
#   Functions should be defined as follows:
#
#   def ODEName(VarVals,t,Params):
#       unpack variables = VarVals
#       unpack nonvariable params = Params[0:-1]
#       unpack variable param = Params[-1]
#
#       duXdt = equations   (x Order of ODE)
#
#       ddt = np.array([all duXdts])
#       return ddt


##### PREDPREY FUNC #########
def PredPrey(VarVals,t,Params):
	x, y = VarVals

	a,d = Params[0:-1] ## NON Variable Params
	b = Params[-1] ## Variable Param

	dxdt = x*(1-x)-(a*x*y)/(d+x)
	dydt = b*y*(1-y/x)
	ddt = np.array([dxdt,dydt])
	return ddt


##### HOPFBI FUNC ######
def HopfBi(VarVals,t,Params):
	u1,u2 = VarVals
	
	sigma = Params[0]
	beta = Params[-1]
	
	du1dt = beta*u1 - u2 + sigma*u1*(u1**2 + u2**2)
	du2dt = u1 + beta*u2 + sigma*u2*(u1**2 + u2**2)
	ddt = np.array([du1dt,du2dt])
	return ddt




################################################
################### SETTINGS ###################
ODE = HopfBi     #Name of ODE function to be used
ODESolver = odeint  #Name of ODE solving function to be used by shoot(), simulate(), naturalparamcont()

PhCondition = True       ##   True if PhC is duXdt=0,
PhCguess = 0.4              ##   False if Constant PhCguess...

Tguess = 4    ## Guess at time period of limit cycle

PlotODESimulated = True ##	True if a plot of initial conditions is wanted

NatParamCont = True ##	True if NatParamCont should be executed for VarParam stated in user input
PlotNatPCont = True ##	True if plot of magnitude ||U|| against VarParam is wanted, to show limit cycle branch


#### TIME RANGE ######
# Defined for simulating ODE over trange, with U0 initial conditions

t0 = 0
tend = 15
dt = 1e-2
trange = np.linspace(t0, tend, int(((tend-t0)/dt)+1))


##### Parameters ####
VarParamStart = 2       # 
VarParamEnd = 0.1         # Define Variable Parameter Range here
Iterations = 101
VarParamRange = np.linspace(VarParamStart,VarParamEnd,Iterations)
VarParamSim = VarParamRange[0]   #Choice of Variable Parameter value for simulation of initial cond. function

P = [-1]         #Non Variable Parameter Values, in list. Unpacked in ODE definition


####VARIABLES#######
Order = 2   #Order of ODE
U0 = [1,0.1]  #ODE Variable initial conditions    (length matches Order)
PhCVar = 1   ## For when duXdt = 0 is phase condition, PhCVar = X here.




#############################
#	Execution Section		#
#############################
if PlotODESimulated == True:
	PSim = P + [VarParamSim] ## Concatenates non variable params with the chosen value of variable param
	UVals = simulate(ODE,ODESolver,U0,trange,PSim)


if NatParamCont == True:

	NatParContSols = naturalparamcont(ODE,ODESolver,Order,U0,Tguess,P,VarParamRange,PhCondition,PhCguess,PhCVar,PlotNatPCont)
	print(NatParContSols)